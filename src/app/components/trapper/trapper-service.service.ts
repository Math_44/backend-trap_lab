import { Denuncia } from './denuncia';
import { HttpClient, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Trappers } from './trappers';


@Injectable({
  providedIn: 'root'
})
export class TrapperServiceService {

  constructor(private http: HttpClient) { };


  private url = "http://localhost:3001/trappers";
  private url2 = "http://localhost:3001/denuncias";
  

 public getTrapper(): Observable<Trappers[]> {

    return this.http.get<Trappers[]>(this.url);


  }


  public getporId(id_trapper:string): Observable<Trappers>{

    const url = `${this.url}/${id_trapper}`;
    return this.http.get<Trappers>(url);


  }
    

 public postTrapper( trapper: Trappers, files: File,url): Observable<Trappers>{

    const formData = new FormData();
    formData.append('trapper_imagem', files, files.name);
    console.log(files.name);
    formData.append('nome', trapper.nome);
    formData.append('musicas', trapper.musicas);
    formData.append('bio', trapper.bio);
    formData.append('canal', trapper.canal);
    

    return this.http.post<Trappers>(url, formData);


  }


  public update(trapper: Trappers): Observable<Trappers>{


    const url = `${this.url}/${trapper.id_trapper}`
    return this.http.patch<Trappers>(url, trapper);


  }


  public postDenuncia(denuncia: Denuncia): Observable<Denuncia>{

    return this.http.post<Denuncia>(this.url2,denuncia);

  }



}
