import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrapperListComponent } from './trapper-list.component';

describe('TrapperListComponent', () => {
  let component: TrapperListComponent;
  let fixture: ComponentFixture<TrapperListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrapperListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrapperListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
