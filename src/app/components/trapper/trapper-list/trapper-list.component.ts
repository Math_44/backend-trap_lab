import { Trappers } from './../trappers';
import { TrapperServiceService } from './../trapper-service.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-trapper-list',
  templateUrl: './trapper-list.component.html',
  styleUrls: ['./trapper-list.component.css']
})
export class TrapperListComponent implements OnInit {



  constructor(private trapperService: TrapperServiceService) { }
  public trappers: Trappers[];


  ngOnInit(){

this.trapperService.getTrapper()
.subscribe(

  (retorno: any) =>{

    console.log(retorno);

    this.trappers = retorno.trappers.map(item => 
      {

      return new Trappers(

        item.id_trapper,
        item.nome,
        item.imagem_trapper,
        item.musicas,
        item.bio, 
        item.canal

    )


    }) 

  }

)


  }


}
