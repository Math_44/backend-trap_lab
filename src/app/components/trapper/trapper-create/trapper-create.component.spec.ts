import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrapperCreateComponent } from './trapper-create.component';

describe('TrapperCreateComponent', () => {
  let component: TrapperCreateComponent;
  let fixture: ComponentFixture<TrapperCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrapperCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrapperCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
