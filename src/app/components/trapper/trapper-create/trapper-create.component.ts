import { Trappers } from './../trappers';
import { Router } from '@angular/router';
import { TrapperServiceService } from './../trapper-service.service';
import { Component, OnInit } from '@angular/core';




@Component({
  selector: 'app-trapper-create',
  templateUrl: './trapper-create.component.html',
  styleUrls: ['./trapper-create.component.css']
})
export class TrapperCreateComponent implements OnInit {

  files: File;


  trapper: Trappers = {

    nome: '',
    musicas: '',
    imagem_trapper: '',
    bio: '',
    canal:''




  }


  constructor(private trapperService: TrapperServiceService, private router: Router) { }



  ngOnInit(): void {
  }

  

  onChange(event) {

    this.files = <File>event.target.files[0];


  }

  


  public createTrapper() {

      console.log(this.files);
    if (this.files) {

      this.trapperService.postTrapper(this.trapper, this.files,  'http://localhost:3001/trappers').subscribe(() => {
        this.router.navigate(['/trapperlist']);

      }


        // (response => console.log('Trapper cadastrado'))

      )

    }

  }


  // public createTrapper(): void {

  //   this.trapperService.postTrapper(this.trapper).subscribe(() => {

  //     this.router.navigate(['/trapperlist']);

  //   })


  // }

  public cancel() {

    this.router.navigate(['/trapperlist']);

  }






}
