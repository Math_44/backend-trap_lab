import { Trappers } from './../trappers';
import { TrapperServiceService } from './../trapper-service.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-trapper-info',
  templateUrl: './trapper-info.component.html',
  styleUrls: ['./trapper-info.component.css']
})
export class TrapperInfoComponent implements OnInit {

  constructor(private router: Router, private route: ActivatedRoute, private trapperService: TrapperServiceService) { }

  public trapper: Trappers;

  ngOnInit(): void {

    const id= this.route.snapshot.paramMap.get('id_trapper');
    
    this.trapperService.getporId(id).subscribe((response: any) =>{

      this.trapper = response.trapper;
      
       console.log(response);
    })

  
    
    


  }


  cancel(){

    this.router.navigate(['/trapperlist']);


  }

}
