import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrapperInfoComponent } from './trapper-info.component';

describe('TrapperInfoComponent', () => {
  let component: TrapperInfoComponent;
  let fixture: ComponentFixture<TrapperInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrapperInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrapperInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
