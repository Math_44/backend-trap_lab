import { TestBed } from '@angular/core/testing';

import { TrapperServiceService } from './trapper-service.service';

describe('TrapperServiceService', () => {
  let service: TrapperServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TrapperServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
