import { Trappers } from './../trappers';
import { TrapperServiceService } from './../trapper-service.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-trapper-update',
  templateUrl: './trapper-update.component.html',
  styleUrls: ['./trapper-update.component.css']
})
export class TrapperUpdateComponent implements OnInit {

  constructor(private router: Router, private actived: ActivatedRoute, private trapperService: TrapperServiceService) { }

  trapper: Trappers;

  ngOnInit(): void {

    const id= this.actived.snapshot.paramMap.get('id_trapper');
    
    this.trapperService.getporId(id).subscribe((response: any) =>{

      this.trapper = response.trapper;
      
       console.log(response);
    })

  }


  updateTrapper():void{

    this.trapperService.update(this.trapper).subscribe((response: any) => {


      return this.router.navigate(['/trapperlist']);


    })

  }




}
