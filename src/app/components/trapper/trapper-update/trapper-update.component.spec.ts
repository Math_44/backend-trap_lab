import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrapperUpdateComponent } from './trapper-update.component';

describe('TrapperUpdateComponent', () => {
  let component: TrapperUpdateComponent;
  let fixture: ComponentFixture<TrapperUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrapperUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrapperUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
