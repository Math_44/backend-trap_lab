import { Denuncia } from './../../trapper/denuncia';
import { TrapperServiceService } from './../../trapper/trapper-service.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-trapper-denuncia',
  templateUrl: './trapper-denuncia.component.html',
  styleUrls: ['./trapper-denuncia.component.css']
})
export class TrapperDenunciaComponent implements OnInit {

denuncias: Denuncia = {

  nome_trapper : '',
  motivo_denuncia: '',
  descricao_denuncia: ''

}


  constructor(private router: Router, private trapperService: TrapperServiceService) { }

  ngOnInit(): void {
  }


  public envDenuncia(){

   return this.trapperService.postDenuncia(this.denuncias).subscribe(() => {
  console.log(this.denuncias)
        this.router.navigate(['/']);
  });

  


  }


}
