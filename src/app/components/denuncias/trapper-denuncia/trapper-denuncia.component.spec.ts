import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrapperDenunciaComponent } from './trapper-denuncia.component';

describe('TrapperDenunciaComponent', () => {
  let component: TrapperDenunciaComponent;
  let fixture: ComponentFixture<TrapperDenunciaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrapperDenunciaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrapperDenunciaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
