import { HeaderComponent } from './components/template/header/header.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NavComponent } from './components/template/nav/nav.component';


import {HttpClientModule} from '@angular/common/http';
import { TrapperListComponent } from './components/trapper/trapper-list/trapper-list.component';
import { HomeComponent } from './components/view/home/home.component';
import { TrapperCreateComponent } from './components/trapper/trapper-create/trapper-create.component';
import {FormsModule} from '@angular/forms';
import { TrapperInfoComponent } from './components/trapper/trapper-info/trapper-info.component';
import { TrapperUpdateComponent } from './components/trapper/trapper-update/trapper-update.component';
import { FooterComponent } from './components/template/footer/footer.component';
import { TrapperDenunciaComponent } from './components/denuncias/trapper-denuncia/trapper-denuncia.component';
import { UsuarioCadastroComponent } from './components/cadastro/usuario-cadastro/usuario-cadastro.component';




@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    TrapperListComponent,
    HomeComponent,
    TrapperCreateComponent,
    HeaderComponent,
    TrapperInfoComponent,
    TrapperUpdateComponent,
    FooterComponent,
    TrapperDenunciaComponent,
    UsuarioCadastroComponent,
    
  
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    HttpClientModule,
    FormsModule
   
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
