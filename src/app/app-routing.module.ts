import { TrapperDenunciaComponent } from './components/denuncias/trapper-denuncia/trapper-denuncia.component';
import { TrapperUpdateComponent } from './components/trapper/trapper-update/trapper-update.component';
import { TrapperInfoComponent } from './components/trapper/trapper-info/trapper-info.component';
import { TrapperCreateComponent } from './components/trapper/trapper-create/trapper-create.component';
import { TrapperListComponent } from './components/trapper/trapper-list/trapper-list.component';
import { NgModule } from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import { HomeComponent } from './components/view/home/home.component';
import { UsuarioCadastroComponent } from './components/cadastro/usuario-cadastro/usuario-cadastro.component';

  

const routes : Routes = [{


path: "",
component: HomeComponent

},

{

path: 'trapperlist',
component: TrapperListComponent

},

{
  
path: "trapper/create",
component: TrapperCreateComponent

},

{

path: "trapper/info/:id_trapper",
component: TrapperInfoComponent

},

{

  path: "trapper/update/:id_trapper",
  component: TrapperUpdateComponent

},

{

  path: "denuncias",
  component: TrapperDenunciaComponent

},

{

path: "cadastro",
component: UsuarioCadastroComponent

}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
